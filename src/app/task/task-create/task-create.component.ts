import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges  } from '@angular/core';
import { callApi } from 'src/app/helpers/apiHelper';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent implements OnInit {

  constructor() { }

  taskForm: FormGroup;
  task: Task = new Task();

  ngOnInit() {
    this.taskForm = new FormGroup({
      'name': new FormControl(this.task.name, [
          Validators.required
      ]),
      'description': new FormControl(this.task.description, [
          Validators.minLength(10)
      ]),
	  'finishedAt': new FormControl(this.task.finishedAt),
    });
  }

  async saveTask() {
    this.task = this.taskForm.value;
	this.task.finishedAt = new Date(this.task.finishedAt).toISOString();
    this.task.createdAt = new Date().toISOString();
    try {
      let endpoint = `tasks`;
      await callApi(endpoint, 'POST', this.task);
    } catch (error) {
      throw error;
    }
    this.taskForm.reset();
  }
}
