import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Task } from 'src/app/models/task';
import { callApi } from 'src/app/helpers/apiHelper';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {

  constructor(private activateRoute: ActivatedRoute, private httpService: HttpService) { 
    this.id = activateRoute.snapshot.params['id'];
  }

  id: number;
  task: Task;
  
  ngOnInit() {
    this.httpService.getData(`tasks/${this.id}`).subscribe((data:Task) => this.task=data);
  }

}
