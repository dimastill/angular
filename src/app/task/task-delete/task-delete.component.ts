import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { Team } from 'src/app/models/team';
import { callApi } from 'src/app/helpers/apiHelper';

@Component({
  selector: 'app-task-delete',
  templateUrl: './task-delete.component.html',
  styleUrls: ['./task-delete.component.css']
})
export class TaskDeleteComponent implements OnInit {

  constructor(private activateRoute: ActivatedRoute) {  
    let id = activateRoute.snapshot.params['id'];
	this.deleteTask(id);
  }

  async deleteTask(id: number) {
    try {
      let endpoint = `tasks/${id}`;
      callApi(endpoint, 'DELETE');
    } catch (error) {
      throw error;
    }
  }
  
  ngOnInit() {
  }

}
