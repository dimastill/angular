import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { callApi } from 'src/app/helpers/apiHelper';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
  constructor(private httpService: HttpService){ }

  tasks: Array<Task>;

  ngOnInit() {
    this.httpService.getData('tasks').subscribe((data:Array<Task>) => this.tasks=data);
  }
}
