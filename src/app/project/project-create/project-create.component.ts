import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges  } from '@angular/core';
import { callApi } from 'src/app/helpers/apiHelper';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Project } from 'src/app/models/project';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit {

  constructor(private httpService: HttpService) {}

  projectForm: FormGroup;
  project: Project = new Project();

  ngOnInit() {
    this.projectForm = new FormGroup({
      'name': new FormControl(this.project.name, [
          Validators.required
      ]),
      'description': new FormControl(this.project.description, [
          Validators.minLength(10)
      ]),
	  'deadline': new FormControl(this.project.deadline),
    });
  }

  async saveProject() {
    this.project = this.projectForm.value;
	this.project.deadline = new Date(this.project.deadline).toISOString();
    this.project.createdAt = new Date().toISOString();
	try {
      let endpoint = `project`;
      await callApi(endpoint, 'POST', this.project);
    } catch (error) {
      throw error;
    }
    this.projectForm.reset();
  }
}
