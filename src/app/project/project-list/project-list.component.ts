import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { callApi } from 'src/app/helpers/apiHelper';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  constructor(private httpService: HttpService){ }

  projects: Array<Project>;

  ngOnInit() {
    this.httpService.getData('project').subscribe((data:Array<Project>) => this.projects=data);
  }

}
