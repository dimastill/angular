import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { callApi } from 'src/app/helpers/apiHelper';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-project-delete',
  templateUrl: './project-delete.component.html',
  styleUrls: ['./project-delete.component.css']
})
export class ProjectDeleteComponent implements OnInit {

  constructor(private activateRoute: ActivatedRoute) {  
    let id = activateRoute.snapshot.params['id'];
	this.deleteProject(id);
  }

  async deleteProject(id: number) {
    try {
      let endpoint = `project/${id}`;
      callApi(endpoint, 'DELETE');
    } catch (error) {
      throw error;
    }
  }
  
  ngOnInit() {
  }

}
