import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { Project } from 'src/app/models/project';
import { callApi } from 'src/app/helpers/apiHelper';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {

  constructor(private activateRoute: ActivatedRoute, private httpService: HttpService) { 
    this.id = activateRoute.snapshot.params['id'];
  }

  id: number;
  project: Project = new Project();

  ngOnInit() {
    this.httpService.getData(`project/${this.id}`).subscribe((data:Project) => this.project=data);
  }
}
