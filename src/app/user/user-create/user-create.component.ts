import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges  } from '@angular/core';
import { callApi } from 'src/app/helpers/apiHelper';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  constructor(private httpService: HttpService) {}

  userForm: FormGroup;
  user: User = new User();

  ngOnInit() {
    this.userForm = new FormGroup({
      'firstName': new FormControl(this.user.firstName, [
          Validators.required
      ]),
      'lastName': new FormControl(this.user.lastName, [
          Validators.required
      ]),
	  'email': new FormControl(this.user.email),
	  'birthday': new FormControl(this.user.birthday),
	  'registeredAt': new FormControl(this.user.registeredAt),
    });
  }

  async saveUser() {
    this.user = this.userForm.value;
	this.user.birthday = new Date(this.user.birthday).toISOString();
    this.user.registeredAt = new Date().toISOString();
	try {
      let endpoint = `users`;
      await callApi(endpoint, 'POST', this.user);
    } catch (error) {
      throw error;
    }
    this.userForm.reset();
  }
}
