import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { callApi } from 'src/app/helpers/apiHelper';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  constructor(private httpService: HttpService){ }

  users: Array<User>;

  ngOnInit() {
    this.httpService.getData('users').subscribe((data:Array<User>) => this.users=data);
  }

}
