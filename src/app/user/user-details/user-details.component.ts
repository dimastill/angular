import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user';
import { callApi } from 'src/app/helpers/apiHelper';
import { ActivatedRoute} from '@angular/router';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  constructor(private activateRoute: ActivatedRoute, private httpService: HttpService) { 
    this.id = activateRoute.snapshot.params['id'];
  }

  id: number = undefined;
  user: User = new User();

  ngOnInit() {
    this.httpService.getData(`users/${this.id}`).subscribe((data:User) => this.user=data);
  }
}
