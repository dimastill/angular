import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { callApi } from 'src/app/helpers/apiHelper';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.css']
})
export class UserDeleteComponent implements OnInit {

  constructor(private activateRoute: ActivatedRoute) {  
    let id = activateRoute.snapshot.params['id'];
	this.deleteUsers(id);
  }

  async deleteUsers(id: number) {
    try {
      let endpoint = `users/${id}`;
      callApi(endpoint, 'DELETE');
    } catch (error) {
      throw error;
    }
  }
  
  ngOnInit() {
  }

}
