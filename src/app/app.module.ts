import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TeamDetailsComponent } from './team/team-details/team-details.component';
import { TeamListComponent } from './team/team-list/team-list.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserDetailsComponent } from './user/user-details/user-details.component';
import { ProjectListComponent } from './project/project-list/project-list.component';
import { ProjectDetailsComponent } from './project/project-details/project-details.component';
import { TaskListComponent } from './task/task-list/task-list.component';
import { TaskDetailsComponent } from './task/task-details/task-details.component';
import { MenuComponent } from './menu/menu.component';
import { AppRoutingModule } from './app-routing.module';
import { TeamCreateComponent } from './team/team-create/team-create.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { ProjectCreateComponent } from './project/project-create/project-create.component';
import { TaskCreateComponent } from './task/task-create/task-create.component';
import { TeamDeleteComponent } from './team/team-delete/team-delete.component';
import { DeleteGuard }   from './delete.guard';
import { ExitGuard }   from './exit.guard';
import { UserDeleteComponent } from './user/user-delete/user-delete.component';
import { ProjectDeleteComponent } from './project/project-delete/project-delete.component';
import { TaskDeleteComponent } from './task/task-delete/task-delete.component';
import { MyDatePipe } from './date.pipe';
import { HttpService } from './http.service';

// @NgModule decorator with its metadata
@NgModule({
  declarations: [AppComponent,
    MenuComponent,
    TeamDetailsComponent,
    TeamListComponent,
    UserListComponent,
    UserDetailsComponent,
    ProjectListComponent,
    ProjectDetailsComponent,
    TaskListComponent,
    TaskDetailsComponent,
    TeamCreateComponent,
    UserCreateComponent,
    ProjectCreateComponent,
    TaskCreateComponent,
    TeamDeleteComponent,
    UserDeleteComponent,
    ProjectDeleteComponent,
    TaskDeleteComponent,
    MyDatePipe],
imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,    
    HttpClientModule,
  ],
  providers: [DeleteGuard, ExitGuard, HttpService],
  bootstrap: [AppComponent]
})
export class AppModule {}