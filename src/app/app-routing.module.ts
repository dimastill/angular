import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamListComponent } from './team/team-list/team-list.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { ProjectListComponent } from './project/project-list/project-list.component';
import { TaskListComponent } from './task/task-list/task-list.component';
import { TeamDetailsComponent } from './team/team-details/team-details.component';
import { UserDetailsComponent } from './user/user-details/user-details.component';
import { ProjectDetailsComponent } from './project/project-details/project-details.component';
import { TaskDetailsComponent } from './task/task-details/task-details.component';
import { TeamCreateComponent } from './team/team-create/team-create.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { ProjectCreateComponent } from './project/project-create/project-create.component';
import { TaskCreateComponent } from './task/task-create/task-create.component';
import { TeamDeleteComponent } from './team/team-delete/team-delete.component';
import { UserDeleteComponent } from './user/user-delete/user-delete.component';
import { ProjectDeleteComponent } from './project/project-delete/project-delete.component';
import { TaskDeleteComponent } from './task/task-delete/task-delete.component';
import { DeleteGuard }   from './delete.guard';
import { ExitGuard }   from './exit.guard';


const routes: Routes = [
  { path: 'teams', component: TeamListComponent },
  { path: 'users', component: UserListComponent },
  { path: 'projects', component: ProjectListComponent },
  { path: 'tasks', component: TaskListComponent },
  { path: 'team/:id', component: TeamDetailsComponent },
  { path: 'user/:id', component: UserDetailsComponent },
  { path: 'project/:id', component: ProjectDetailsComponent },
  { path: 'task/:id', component: TaskDetailsComponent },
  { path: 'newTeam', component: TeamCreateComponent, canDeactivate: [ExitGuard] },
  { path: 'newUser', component: UserCreateComponent, canDeactivate: [ExitGuard] },
  { path: 'newProject', component: ProjectCreateComponent, canDeactivate: [ExitGuard] },
  { path: 'newTask', component: TaskCreateComponent, canDeactivate: [ExitGuard] },
  { path: 'deleteTeam/:id', component: TeamDeleteComponent, canActivate: [DeleteGuard]},
  { path: 'deleteUser/:id', component: UserDeleteComponent, canActivate: [DeleteGuard]},
  { path: 'deleteProject/:id', component: ProjectDeleteComponent, canActivate: [DeleteGuard]},
  { path: 'deleteTask/:id', component: TaskDeleteComponent, canActivate: [DeleteGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
