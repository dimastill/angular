import { Pipe, PipeTransform } from '@angular/core';
 
@Pipe({
    name: 'myDatePipe'
})
export class MyDatePipe implements PipeTransform {
  transform(value: string, args?: any): string {
   let options = { year: 'numeric', month: 'long', day: 'numeric' };
   return new Date(value).toLocaleDateString('uk', options);
  }
}