import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import { callApi } from 'src/app/helpers/apiHelper';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-team-delete',
  templateUrl: './team-delete.component.html',
  styleUrls: ['./team-delete.component.css']
})
export class TeamDeleteComponent implements OnInit {

  constructor(private activateRoute: ActivatedRoute) {  
    let id = activateRoute.snapshot.params['id'];
	this.deleteTeams(id);
  }

  async deleteTeams(id: number) {
    try {
      let endpoint = `teams/${id}`;
      callApi(endpoint, 'DELETE');
    } catch (error) {
      throw error;
    }
  }
  
  ngOnInit() {
  }
}
