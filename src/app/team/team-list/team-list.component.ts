import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/models/team';
import { callApi } from 'src/app/helpers/apiHelper';
import { HttpClient } from '@angular/common/http';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {
  constructor(private httpService: HttpService){ }

  teams: Array<Team>;
  
  ngOnInit() {
    this.httpService.getData('teams').subscribe((data:Array<Team>) => this.teams=data);
  }

}
