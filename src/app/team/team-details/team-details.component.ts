import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { Team } from 'src/app/models/team';
import { callApi } from 'src/app/helpers/apiHelper';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit {

  constructor(private activateRoute: ActivatedRoute, private httpService: HttpService) { 
    this.id = activateRoute.snapshot.params['id'];
  }

  id: number = undefined;
  team: Team = new Team();

  ngOnInit() {
    this.httpService.getData(`teams/${this.id}`).subscribe((data:Team) => this.team=data);
  }
}
