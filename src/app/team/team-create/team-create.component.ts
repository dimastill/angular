import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges  } from '@angular/core';
import { callApi } from 'src/app/helpers/apiHelper';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Team } from 'src/app/models/team';
import { ExitGuard, ComponentCanDeactivate } from 'src/app/exit.guard';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css']
})
export class TeamCreateComponent implements OnInit, ComponentCanDeactivate {

  constructor(private httpService: HttpService) {}

  teamForm: FormGroup;
  team: Team = new Team();

  ngOnInit() {
    this.teamForm = new FormGroup({
      'name': new FormControl(this.team.name, [
          Validators.required,
          Validators.minLength(3)
      ]),
      'createdAt': new FormControl(this.team.createdAt)
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
    if(changes.team && this.teamForm){
        this.teamForm.setValue(changes.team.currentValue)
    }
}

  async saveTeam() {
    this.team = this.teamForm.value;
    this.team.createdAt = new Date().toISOString();
    try {
      let endpoint = `teams`;
      await callApi(endpoint, 'POST', this.team);
    } catch (error) {
      throw error;
    }
    this.teamForm.reset();
  }

  canDeactivate() : boolean | Observable<boolean>{
	  return confirm("Вы хотите покинуть страницу?");
  }
}
