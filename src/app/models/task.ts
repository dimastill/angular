import { Project } from './project';
import { User } from './user';

enum State {
    Created,
    Started,
    Finished,
    Canceled        
}

export class Task {
    id: number;
    name: string;
    description: string;
    createdAt: string;
    finishedAt: string;
    state: State;
    project: Project;
    perfomer: User;
}