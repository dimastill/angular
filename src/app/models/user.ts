import { Team } from './team';

export class User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    birthday: string;
    registeredAt: string;
    team: Team;
}