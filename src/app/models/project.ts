import { User } from './user';
import { Team } from './team';

export class Project {
    id: number;
    name: string;
    description: string;
    createdAt: string;
    deadline: string;
    author: User;
    team: Team;
}