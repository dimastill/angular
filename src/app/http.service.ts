import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
  
@Injectable()
export class HttpService{
  
    constructor(private http: HttpClient){ }
      
    getData(endPoint: string){
        return this.http.get(`http://localhost:5000/api/${endPoint}`);
    }

    postData(endPoint: string, data: any) {
        return this.http.post(`http://localhost:5000/api/${endPoint}`, data);
    }

    deleteData(endPoint: string) {
        return this.http.delete(`http://localhost:5000/api/${endPoint}`);
    }
}