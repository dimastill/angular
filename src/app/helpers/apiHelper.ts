const API_URL = 'http://localhost:5000/api/';

async function callApi(endpoind: string, method: string, data = undefined) {
  const url = API_URL + endpoind;
  const options: RequestInit = {
    method,
    body: JSON.stringify(data),
    
  };
  
  if (data) {
      options.headers = {
        'Content-Type': 'application/json',
      };
  }

  try {
        const response = await fetch(url, options);
        if(response.ok)
          return await response.json(); 
        else
          return Promise.reject(Error('Failed to load'));
    }
    catch (error) {
        throw error;
    }
}

export { callApi }