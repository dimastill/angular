﻿using ProjectStructureTask.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Command
{
    public class DeleteTeamCommand : ICommand<bool>
    {
        public int Id { get; set; }
    }
}
