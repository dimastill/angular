﻿using System;
using ProjectStructureTask.Services.DTOs;
using ProjectStructureTask.Services.Abstractions;

namespace ProjectStructureTask.Services.Command
{
    public class AddTaskCommand : ICommand<bool>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskStateModel State { get; set; }
        public Project Project { get; set; }
        public User Perfomer { get; set; }
    }
}
