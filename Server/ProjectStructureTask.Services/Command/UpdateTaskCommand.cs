﻿using ProjectStructureTask.Services.Abstractions;
using System;
using ProjectStructureTask.Services.DTOs;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Command
{
    public class UpdateTaskCommand : ICommand<bool>
    {
        public int UpdateId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskStateModel State { get; set; }
        public Project Project { get; set; }
        public User Perfomer { get; set; }
    }
}
