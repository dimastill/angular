﻿using AutoMapper;
using ProjectStructureTask.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using Threading = System.Threading.Tasks;

namespace ProjectStructureTask.Services.Handlers
{
    public class ToDoCommandHandler : IToDoCommandHandler
    {
        private readonly UnitOfWork dataContext;
        private readonly IMapper mapper;
        private readonly IQueueService queueService;

        public ToDoCommandHandler(IUnitOfWork dataContext, IMapper mapper)
        {
            this.dataContext = dataContext as UnitOfWork;
            this.mapper = mapper;
        }
        public Dictionary<Type, Func<object, Threading.Task<object>>> GetHandlers()
        {
            return new Dictionary<Type, Func<object, Threading.Task<object>>>
            {
                { typeof(AddProjectCommand), async t => await HandleAsync(t as AddProjectCommand) },
                { typeof(AddTeamCommand), async t => await HandleAsync(t as AddTeamCommand) },
                { typeof(AddUserCommand), async t => await HandleAsync(t as AddUserCommand) },
                { typeof(AddTaskCommand), async t => await HandleAsync(t as AddTaskCommand) },
                { typeof(UpdateProjectCommand), async t => await HandleAsync(t as UpdateProjectCommand) },
                { typeof(UpdateTeamCommand), async t => await HandleAsync(t as UpdateTeamCommand) },
                { typeof(UpdateUserCommand), async t => await HandleAsync(t as UpdateUserCommand) },
                { typeof(UpdateTaskCommand), async t => await HandleAsync(t as UpdateTaskCommand) },
                { typeof(DeleteProjectCommand), async t => await HandleAsync(t as DeleteProjectCommand) },
                { typeof(DeleteUserCommand), async t => await HandleAsync(t as DeleteUserCommand) },
                { typeof(DeleteTeamCommand), async t => await HandleAsync(t as DeleteTeamCommand) },
                { typeof(DeleteTaskCommand), async t => await HandleAsync(t as DeleteTaskCommand) },
            };
        }

        private async Threading.Task<bool> HandleAsync(AddProjectCommand command)
        {
            command.Id = dataContext.Projects.Last().Id + 1;

            var project = mapper.Map<AddProjectCommand, Project>(command);
            await Threading.Task.Run(() => dataContext.Projects.Add(project));

            return true;
        }

        private async Threading.Task<bool> HandleAsync(AddTeamCommand command)
        {
            
            command.Id = dataContext.Teams.Last().Id + 1;
			
            var team = mapper.Map<AddTeamCommand, Team>(command);
            await Threading.Task.Run(() => dataContext.Teams.Add(team));

            return true;
        }

        private async Threading.Task<bool> HandleAsync(AddUserCommand command)
        {
            
            command.Id = dataContext.Users.Last().Id + 1;

            var user = mapper.Map<AddUserCommand, User>(command);

            await Threading.Task.Run(() => dataContext.Users.Add(user));

            return true;
        }

        private async Threading.Task<bool> HandleAsync(AddTaskCommand command)
        {
            
            command.Id = dataContext.Tasks.Last().Id + 1;

            var task = mapper.Map<AddTaskCommand, Task>(command);
            await Threading.Task.Run(() => dataContext.Tasks.Add(task));

            return true;
        }

        private async Threading.Task<bool> HandleAsync(UpdateProjectCommand command)
        {
            var project = await Threading.Task.Run(() => 
                dataContext.Projects.Single(updateProject => updateProject.Id == command.UpdateId));

            if (project == null)
                return false;

            mapper.Map(command, project);

            return true;
        }

        private async Threading.Task<bool> HandleAsync(UpdateTeamCommand command)
        {
            var team = await Threading.Task.Run(() => 
                dataContext.Teams.Single(updateTeam => updateTeam.Id == command.UpdateId));

            if (team == null)
                return false;

            mapper.Map(command, team);

            return true;
        }

        private async Threading.Task<bool> HandleAsync(UpdateUserCommand command)
        {
            var user = await Threading.Task.Run(() => 
                dataContext.Users.Single(updateUser => updateUser.Id == command.UpdateId));

            if (user == null)
                return false;

            mapper.Map(command, user);

            return true;
        }

        private async Threading.Task<bool> HandleAsync(UpdateTaskCommand command)
        {
            var task = dataContext.Tasks.Single(updateTask => 
                updateTask.Id == command.UpdateId);

            if (task == null)
                return false;

            mapper.Map(command, task);

            return true;
        }

        private async Threading.Task<bool> HandleAsync(DeleteProjectCommand command)
        {
            var project = await Threading.Task.Run(() => 
                dataContext.Projects.Single(deleteProject => deleteProject.Id == command.Id));

            if (project == null)
                return false;

            await Threading.Task.Run(() => dataContext.Projects.Remove(project));

            return true;
        }

        private async Threading.Task<bool> HandleAsync(DeleteTeamCommand command)
        {
            var team = await Threading.Task.Run(() => 
                dataContext.Teams.Single(deleteTeam => deleteTeam.Id == command.Id));

            if (team == null)
                return false;

            await Threading.Task.Run(() => dataContext.Teams.Remove(team));

            return true;
        }

        private async Threading.Task<bool> HandleAsync(DeleteUserCommand command)
        {
            var user = await Threading.Task.Run(() => 
                dataContext.Users.SingleOrDefault(deleteUser => deleteUser.Id == command.Id));

            if (user == null)
                return false;

            await Threading.Task.Run(() => dataContext.Users.Remove(user));

            return true;
        }

        private async Threading.Task<bool> HandleAsync(DeleteTaskCommand command)
        {
            var task = await Threading.Task.Run(() => 
                dataContext.Tasks.SingleOrDefault(deleteTask => deleteTask.Id == command.Id));

            if (task == null)
                return false;

            await Threading.Task.Run(() => dataContext.Tasks.Remove(task));

            return true;
        }
    }
}
