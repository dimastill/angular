﻿using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Handlers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Implementation
{
    public class ToDoCommandProcessor : Processor, ICommandProcessor
    {
        public ToDoCommandProcessor(IToDoCommandHandler handlerFactory)
        {
            RegisterHandlersAsync(handlerFactory);
        }
    }
}
