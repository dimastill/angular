﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructureTask.Services.Repositories
{
    public class Repository<T> : IRepository<T>, ICollection<T>
        where T : class
    {
        List<T> Items { get; } = new List<T>();

        public int Count
        {
            get
            {
                return Items.Count;
            }
        }

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(T item)
        {
            Items.Add(item);
        }

        public void AddRange(ICollection<T> items)
        {
            Items.AddRange(items);
        }

        public void Delete(int indexItem) 
        {
            Items.RemoveAt(indexItem);
        }

        public void Delete(T item)
        {
            Items.Remove(item);
        }

        public T Find(T findItem)
        {
            return Items.Find(item => item == findItem);
        }

        public T Find(Predicate<T> match)
        {
            return Items.Find(match);
        }

        public async Task<List<T>> ToListAsync()
        {
            return await Task.Run(() => Items);
        }

        public int IndexOf(T item)
        {
            return Items.IndexOf(item);
        }

        public void Clear()
        {
            Items.Clear();
        }

        public bool Contains(T item)
        {
            return Items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Items.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return Items.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        public T this[int index]
        {
            get
            {
                return Items[index];
            }
            set
            {
                Items[index] = value;
            }
        }
    }
}
