﻿﻿using Microsoft.EntityFrameworkCore;
using ProjectStructureTask.Services.DTOs;
using System;
using System.Collections.Generic;

namespace ProjectStructureTask.Services.Repositories
{
    public class UnitOfWork : DbContext, IUnitOfWork
    {
        Repository<User> users = new Repository<User>();
        public Repository<Project> Projects { get; set; } = new Repository<Project>();
        public Repository<Task> Tasks { get; set; } = new Repository<Task>();
        public Repository<User> Users
        {
            get { return users; }
            set { users = value; }
        } 
        public Repository<Team> Teams { get; set; } = new Repository<Team>();

        // Initialization data
        public UnitOfWork()
        {
            Random random = new Random();

            // Generate teams
            if (Users.Count == 0)
            {
                var team = new Team { Id = 1, CreatedAt = DateTime.Parse("27.07.1968"), Name = "TeamNameTest0" };
                Teams.Add(team);
            }

            //Generate users
            if (Users.Count == 0)
            {
                    var users = new List<User>
                    {
                        new User { Id = 1, FirstName = "FirstNameTest0", LastName = "LastNameTest0",
                            Email = "EmailTest0@email.com", Birthday = DateTime.Parse("21.05.1972"), Team = Teams[0]
                        },
                        new User { Id = 2, FirstName = "FirstNameTest1", LastName = "LastNameTest1",
                            Email = "EmailTest1@email.com", Birthday = DateTime.Parse("12.01.2005"), Team = Teams[0]
                        },
                        new User { Id = 3, FirstName = "FirstNameTest2", LastName = "LastNameTest2",
                            Email = "EmailTest2@email.com", Birthday = DateTime.Parse("13.01.2005"), Team = null
                        },
                    };

                    Users.AddRange(users);
            }

            //Generate projects
            if (Projects.Count == 0)
            {
                var projects = new List<Project>
                {
                    new Project { Id = 1, Name = "NameTest0", CreatedAt = DateTime.Parse("03.06.1962"),
                        Deadline = DateTime.Parse("23.09.1970"), Description = "DescriptionTest0",
                        Team = Teams[0], Author = Users[0]
                    },
                    new Project { Id = 2, Name = "NameTest1", CreatedAt = DateTime.Parse("30.12.1979"),
                        Deadline = DateTime.Parse("22.06.1986"), Description = "DescriptionTest1",
                        Team = Teams[0], Author = Users[1]
                    },
                    new Project { Id = 3, Name = "NameTest2", CreatedAt = DateTime.Parse("03.07.2001"),
                        Deadline = DateTime.Parse("03.10.1971"), Description = "DescriptionTest2",
                        Team = Teams[0], Author = Users[0]
                    },
                };

                Projects.AddRange(projects);
            }

            //Generate tasks
            if (Tasks.Count == 0)
            {
                var tasks = new List<Task>
                {
                    new Task { Id = 1, Description = "DescriptionTest0", Name = "NameTest0",
                        CreateAt = DateTime.Parse("25.08.1987"), FinishedAt = DateTime.Parse("05.11.1988"),
                        Perfomer = Users[0], Project = Projects[0], State = new TaskStateModel { Id = 0, Value = "Created" }
                    },
                    new Task { Id = 2, Description = "DescriptionTest1", Name = "NameTest1",
                        CreateAt = DateTime.Parse("10.06.1997"), FinishedAt = DateTime.Parse("23.03.2005"),
                        Perfomer = Users[0], Project = Projects[1], State = new TaskStateModel { Id = 0, Value = "Started" }
                    },
                    new Task { Id = 3, Description = "DescriptionTest2", Name = "NameTest2",
                        CreateAt = DateTime.Parse("17.08.1962"), FinishedAt = DateTime.Parse("12.03.1969"),
                        Perfomer = Users[1], Project = Projects[2], State = new TaskStateModel { Id = 0, Value = "Finished" }
                    },
                    new Task { Id = 4, Description = "DescriptionTest3", Name = "NameTest3",
                        CreateAt = DateTime.Parse("24.09.1972"), FinishedAt = DateTime.Parse("15.06.1990"),
                        Perfomer = Users[1], Project = Projects[0], State = new TaskStateModel { Id = 0, Value = "Canceled" }
                    }
                };

                Tasks.AddRange(tasks);
            }
        }
    }
}
