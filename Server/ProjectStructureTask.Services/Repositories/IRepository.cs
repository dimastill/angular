﻿using System;
using System.Collections.Generic;
using Threading = System.Threading.Tasks;

namespace ProjectStructureTask.Services.Repositories
{
    public interface IRepository<T> where T : class
    {
        void Add(T item);
        void Delete(int id);
        void Delete(T item);
        T Find(T item);
        T Find(Predicate<T> match);
        Threading.Task<List<T>> ToListAsync();
        int IndexOf(T item);
    }
}
