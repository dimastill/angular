﻿using System;

namespace ProjectStructureTask.Services.DTOs
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskStateModel State { get; set; }
        public Project Project { get; set; }
        public User Perfomer { get; set; }
    }
}
