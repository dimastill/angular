﻿namespace ProjectStructureTask.Services.DTOs
{
    public class TaskStateModel
    {
        public long Id { get; set; }
        public string Value { get; set; }

        public static implicit operator TaskStateModel(long Id)
        {
            switch (Id)
            {
                case 0:
                    return new TaskStateModel { Id = Id, Value = "Created" };
                case 1:
                    return new TaskStateModel { Id = Id, Value = "Started" };
                case 2:
                    return new TaskStateModel { Id = Id, Value = "Finished" };
                case 3:
                    return new TaskStateModel { Id = Id, Value = "Canceled" };
                default:
                    return null;
            }
        }
    }
}
