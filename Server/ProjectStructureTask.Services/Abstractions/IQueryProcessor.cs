﻿using System.Threading.Tasks;

namespace ProjectStructureTask.Services.Abstractions
{
    public interface IQueryProcessor
    {
        Task<TResult> ProcessAsync<TResult>(IQuery<TResult> query);
    }
}