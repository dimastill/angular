﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructureTask.Services.Abstractions
{
    public interface IQueueService
    {
        Task<bool> PostValue(string value);
    }
}
