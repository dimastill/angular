﻿using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Queries
{
    public class GetListTasksQuery : IQuery<List<Task>>
    {
        public int Id { get; set; }
    }
}
