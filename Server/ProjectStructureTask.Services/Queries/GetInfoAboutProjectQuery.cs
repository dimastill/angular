﻿using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Queries
{
    public class GetInfoAboutProjectQuery : IQuery<IEnumerable<object>>
    {
        public int Id { get; set; }
    }
}
