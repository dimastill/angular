﻿using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Queries
{
    public class GetNumberTasksInProjectQuery : IQuery<Dictionary<string, int>>
    {
        public int Id { get; set; }
    }
}
