﻿using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Queries
{
    public class GetInfoAboutLastProjectQuery : IQuery<IEnumerable<object>>
    {
        public int Id { get; set; }
    }
}
