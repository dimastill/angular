﻿using Newtonsoft.Json;
using ProjectStructureTask.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Timers;
using System.Text;
using Microsoft.AspNetCore.SignalR.Client;
using Threading = System.Threading.Tasks;

namespace Client
{
    static class Data
    {
        private static string serverAppPath = "http://localhost:5000/api";

        static HttpClient client = new HttpClient();

        public async static Threading.Task<string> GetJson(string uri)
        {
            HttpResponseMessage response = await client.GetAsync($"{uri}");
            string json = await response.Content.ReadAsStringAsync();

            return json;
        }

        public async static Threading.Task<Dictionary<string, int>> GetNumberTasks(int userId)
        {
            string json = await GetJson($"{serverAppPath}/Users/TasksInProjectForUser/{userId}");

            return JsonConvert.DeserializeObject<Dictionary<string, int>>(json);
        }

        public async static Threading.Task<List<Task>> GetListTasks(int userId)
        {
            string json = await GetJson($"{serverAppPath}/Tasks/ForUser/{userId}");

            return JsonConvert.DeserializeObject<List<Task>>(json);
        }

        public async static Threading.Task<List<Task>> GetListTasksFinished2019(int userId)
        {
            string json = await GetJson($"{serverAppPath}/Tasks/FinishedIn2019/{userId}");

            return JsonConvert.DeserializeObject<List<Task>>(json);
        }

        public async static Threading.Task<IEnumerable<dynamic>> GetOldUsers()
        {
            string json = await GetJson($"{serverAppPath}/Teams/OldUsers");

            return JsonConvert.DeserializeObject<IEnumerable<dynamic>>(json);
        }

        public async static Threading.Task<IEnumerable<Task>> GetSortedUsersList()
        {
            string json = await GetJson($"{serverAppPath}/Users/sorted");

            return JsonConvert.DeserializeObject<IEnumerable<Task>>(json);
        }
        
        public async static Threading.Task<IEnumerable<dynamic>> GetInfoAboutTasks(int userId)
        {
            string json = await GetJson($"{serverAppPath}/Project/LastProject/{userId}");

            return JsonConvert.DeserializeObject<IEnumerable<dynamic>>(json);
        }

        public async static Threading.Task<IEnumerable<dynamic>> GetInfoAboutProject(int projectId)
        {
            string json = await GetJson($"{serverAppPath}/Project/GetInfoAboutProject/{projectId}");

            return JsonConvert.DeserializeObject<IEnumerable<dynamic>>(json);
        }

        public async static Threading.Task<IEnumerable<Team>> GetAllTeams()
        {
            string json = await GetJson($"{serverAppPath}/Teams");

            return JsonConvert.DeserializeObject<IEnumerable<Team>>(json);
        }

        public async static Threading.Task<IEnumerable<User>> GetAllUsers()
        {
            string json = await GetJson($"{serverAppPath}/Users");

            return JsonConvert.DeserializeObject<IEnumerable<User>>(json);
        }

        public async static Threading.Task<IEnumerable<Project>> GetAllProjects()
        {
            string json = await GetJson($"{serverAppPath}/Project");

            return JsonConvert.DeserializeObject<IEnumerable<Project>>(json);
        }

        public async static Threading.Task<IEnumerable<Task>> GetAllTasks()
        {
            string json = await GetJson($"{serverAppPath}/Tasks");

            return JsonConvert.DeserializeObject<IEnumerable<Task>>(json);
        }

        public async static Threading.Task<Team> GetTeam(int id)
        {
            string json = await GetJson($"{serverAppPath}/Team/{id}");

            return JsonConvert.DeserializeObject<Team>(json);
        }

        public async static Threading.Task<User> GetUser(int id)
        {
            string json = await GetJson($"{serverAppPath}/User/{id}");

            return JsonConvert.DeserializeObject<User>(json);
        }

        public async static Threading.Task<Project> GetProject(int id)
        {
            string json = await GetJson($"{serverAppPath}/Project/{id}");

            return JsonConvert.DeserializeObject<Project>(json);
        }

        public async static Threading.Task<Task> GetTask(int id)
        {
            string json = await GetJson($"{serverAppPath}/Tasks/{id}");

            return JsonConvert.DeserializeObject<Task>(json);
        }

        public async static Threading.Task<HttpResponseMessage> Post(string method, object obj)
        {
            string jsonObject = JsonConvert.SerializeObject(obj);

            var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");

            return await client.PutAsync($"{serverAppPath}/{method}", content);
        }

        public async static Threading.Task<HttpResponseMessage> Put(string modelName, int id, object obj)
        {
            string jsonObject = JsonConvert.SerializeObject(obj);

            var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");

            return await client.PutAsync($"{serverAppPath}/{modelName}/{id}", content);
        }

        public async static Threading.Task<HttpResponseMessage> Delete(string method, int id)
        {
            return await client.DeleteAsync($"{serverAppPath}/{method}/{id}");
        }
    }
}
