﻿using ProjectStructureTask.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Client
{
    static class Menu
    {
        public static void Display()
        {
            Console.WriteLine("1. Отримати кiлькiсть таскiв у проектi конкретного користувача (по id)\n" +
                              "2. Отримати список таскiв, призначених для конкретного користувача (по id), де name таска " +
                              "<45 символiв\n" +
                              "3. Отримати список (id, name) з колекцiї таскiв, якi виконанi (finished) в поточному (2019) " +
                              "роцi для конкретного користувача (по id)\n" +
                              "4. Отримати список (id, iм'я команди i список користувачiв) з колекцiї команд, учасники яких " +
                              "старшi 12 рокiв, вiдсортованих за датою реєстрацiї користувача за спаданням, а також " +
                              "згрупованих по командах.\n" +
                              "5. Отримати список користувачiв за алфавiтом first_name (по зростанню) " +
                              "з вiдсортованими tasks по довжинi name (за спаданням)\n" +
                              "6. Отримати наступну структуру (передати Id користувача в параметри):\n" +
                              "\tUser\n" +
                              "\tОстаннiй проект користувача(за датою створення)\n" +
                              "\tЗагальна кiлькiсть таскiв пiд останнiм проектом\n" +
                              "\tЗагальна кiлькiсть незавершених або скасованих таскiв для користувача\n" +
                              "\tНайтривалiший таск користувача за датою(найранiше створений - найпiзнiше закiнчений)\n" +
                              "7. Отримати таку структуру (передати Id проекту в параметри):\n" +
                              "\tПроект\n" +
                              "\tНайдовший таск проекту(за описом)\n" +
                              "\tНайкоротший таск проекту(по iменi)\n" +
                              "\tЗагальна кiлькiсть користувачiв в командi проекту, де або опис проекту > 25 символiв, " +
                              "або кiлькiсть таскiв < 3");
        }

        public static int InputMenuItem()
        {
            Console.Write("Виберiть iндекс елемента меню: ");
            int menuItem = Convert.ToInt32(Console.ReadLine());

            return menuItem;
        }

        public static int InputUserId()
        {
            Console.Write("Введiть id користувача: ");
            int Id = Convert.ToInt32(Console.ReadLine());

            return Id;
        }

        public static int InputProjectId()
        {
            Console.Write("Введiть id проекту ");
            int Id = Convert.ToInt32(Console.ReadLine());

            return Id;
        }

        public async static void DisplayNumberTasksByUserId()
        {
            int Id = InputUserId();

            Dictionary<string, int> numberTasks = await Data.GetNumberTasks(Id);

            Console.WriteLine($"Кiлькiсть завдань у проектах для користувача з id {Id}:");
            foreach (var project in numberTasks)
            {
                Console.WriteLine($"Проект: {project.Key} Кiлькiсть таскiв: {project.Value}");
            }
        }

        public async static void DisplayListTasksByUserId()
        {
            int Id = InputUserId();

            List<Task> tasks = await Data.GetListTasks(Id);

            foreach (var task in tasks)
            {
                Console.WriteLine($"Завдання #{task.Id}: {task.Name}");
            }
        }

        public async static void DisplayTasksFinishedIn2019()
        {
            int Id = InputUserId();

            var tasksFinishedIn2019 = await Data.GetListTasksFinished2019(Id);
            foreach (var task in tasksFinishedIn2019)
            {
                Console.WriteLine($"Id #{task.Id}. Назва таскiв: {task.Name}");
            }
        }

        public async static void DisplayUsersOlderThan12YearsOld()
        {
            foreach (var oldUser in await Data.GetOldUsers())
            {
                Console.Write($"Команда #{oldUser.id} {oldUser.name}\nУчасники:\n");
                foreach (var teamUser in oldUser.users)
                {
                    Console.WriteLine($"{teamUser.firstName} {teamUser.lastName} " +
                        $"(Дата народження: {teamUser.birthday.ToString("dd/MM/yyyy")}. " +
                        $"Дата реєстрацiї: {teamUser.registeredAt.ToString("dd/MM/yyyy")})");
                }
            }
        }

        public async static void DisplaySortedUsersList()
        {
            foreach (var sortedItem in await Data.GetSortedUsersList())
            {
                Console.WriteLine($"{sortedItem.Perfomer.FirstName} {sortedItem.Perfomer.LastName} " +
                                  $"(Таск: {sortedItem.Name})");
            }
        }

        public async static void DisplayInfoAboutUser()
        {
            int Id = InputUserId();

            var response = await Data.GetInfoAboutTasks(Id);
            var result = response.FirstOrDefault();
            Console.WriteLine($"Користувач #{result.user.id}\n" +
                $"Останнiй проект #{result.lastProject.id}\n" +
                $"(Кiлькiсть таскiв: {result.numberTasksInLastProject}. " +
                $"Незавершених/Скасованих: {result.numberUnfinishedOrCanceledTasks})\n" +
                $"Найтривалiший таск #{result.longTask.id}");
        }

        public async static void DisplayInfoAboutProject()
        {
            int Id = InputProjectId();

            try
            {
                var responseAboutProject = await Data.GetInfoAboutProject(Id);
                var infoAboutProject = responseAboutProject.FirstOrDefault();
                Console.WriteLine($"Проект #{infoAboutProject.project.id}\n" +
                    $"Найдовший таск (за описом) #{infoAboutProject.longDescriptionProject.id}\n" +
                    $"Найкоротший таск (по iменi) #{infoAboutProject.shortNameTask.id}\n" +
                    $"Кiлькiсть користувачiв в командi даного проекту: {infoAboutProject.numberUsersInProject}");
            }
            catch
            {
                Console.WriteLine("Проект не має таскiв");
            }
        }

        public static void Wait()
        { 
            Console.WriteLine("\nНатиснiть будь-яку клавiшу для повернення до меню");
            Console.ReadKey();
            Console.Clear();
        }

        public static void DisplayCRUD()
        {
            Console.WriteLine("Функiї CRUD:\n1. Create (POST)\n2. Read (GET)\n3. Update (PUT)\n4. Delete (DELETE)");

            switch (InputMenuItem())
            {
                case 1:
                    break;
                case 2:
                    DisplayGET();
                    break;
                case 3:
                    break;
                case 4:
                    break;

            }
        }

        public static void DisplayGET()
        {
            Console.WriteLine("1. Teams\n2.Users\n3.Projects\n4.Tasks");
            var menuItem = InputMenuItem();
            switch (menuItem)
            {
                case 1:
                    DisplayTeams();
                    break;
                case 2:
                    DisplayUsers();
                    break;
                case 3:
                    DisplayProjects();
                    break;
                case 4:
                    DisplayTasks();
                    break;
            }
        }

        public static void DisplayPOST()
        {
            Console.WriteLine("1. Teams\n2.Users\n3.Projects\n4.Tasks");
            var menuItem = InputMenuItem();
            switch (menuItem)
            {
                case 1:
                    DisplayPostTeam();
                    break;
                case 2:
                    DisplayPostUser();
                    break;
                case 3:
                    DisplayPostProject();
                    break;
                case 4:
                    DisplayPostTask();
                    break;
            }
        }

        public async static void DisplayTeams()
        {
            foreach (var team in await Data.GetAllTeams())
                Console.WriteLine($"Команда #{team.Id}: {team.Name}");
        }

        public async static void DisplayUsers()
        {
            foreach (var user in await Data.GetAllUsers())
                Console.WriteLine($"Користувач #{user.Id}: {user.FirstName} {user.LastName}");
        }

        public async static void DisplayProjects()
        {
            foreach (var project in await Data.GetAllProjects())
                Console.WriteLine($"Користувач #{project.Id}: {project.Name}");
        }

        public async static void DisplayTasks()
        {
            foreach (var task in await Data.GetAllTasks())
                Console.WriteLine($"Таск #{task.Id}: {task.Name}");
        }

        public async static void DisplayPostTeam()
        {
            Console.Write("Enter team name: ");
            string Name = Console.ReadLine();

            Team team = new Team { Name = Name, CreatedAt = DateTime.Now };

            await Data.Post("Teams", team);
        }

        public async static void DisplayPostUser()
        {
            Console.Write("Enter first name: ");
            string firstName = Console.ReadLine();
            Console.Write("Enter last name: ");
            string lastName = Console.ReadLine();
            Console.Write("Enter email: ");
            string email = Console.ReadLine();
            Console.Write("Enter birthday: ");
            DateTime birthday = DateTime.Parse(Console.ReadLine());
            Console.Write("Enter user team id: ");
            int Id = Convert.ToInt32(Console.ReadLine());
            Team userTeam = await Data.GetTeam(Id);

            User user = new User
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Birthday = birthday,
                RegisteredAt = DateTime.Now,
                Team = userTeam
            };

            await Data.Post("Users", user);
        }

        public async static void DisplayPostProject()
        {
            Console.Write("Enter project name: ");
            string Name = Console.ReadLine();
            Console.Write("Enter description: ");
            string projectDescription = Console.ReadLine();
            Console.Write("Enter deadline: ");
            DateTime projectDeadline = DateTime.Parse(Console.ReadLine());
            Console.Write("Enter project team id: ");
            int projectId = Convert.ToInt32(Console.ReadLine());
            Team projectTeam = await Data.GetTeam(projectId);
            Console.Write("Enter project author id: ");
            int userId = Convert.ToInt32(Console.ReadLine());
            User projectAuthor = await Data.GetUser(userId);

            Project project = new Project
            {
                Name = Name,
                Description = projectDescription,
                Deadline = projectDeadline,
                CreatedAt = DateTime.Now,
                Team = projectTeam,
                Author = projectAuthor
            };

            await Data.Post("Project", project);
        }

        public async static void DisplayPostTask()
        {
            Console.Write("Enter task name: ");
            string Name = Console.ReadLine();
            Console.Write("Enter decription: ");
            string description = Console.ReadLine();
            Console.Write("Enter creation date: ");
            DateTime creationDate = DateTime.Parse(Console.ReadLine());
            Console.Write("Enter perfomer id: ");
            int userId = Convert.ToInt32(Console.ReadLine());
            User user = await Data.GetUser(userId);
            Console.Write("Enter project id: ");
            int projectId = Convert.ToInt32(Console.ReadLine());
            Project project = await Data.GetProject(projectId);

            Task task = new Task
            {
                Name = Name,
                Description = description,
                CreateAt = creationDate,
                State = new TaskStateModel { Id = 0, Value = "Created" },
                Perfomer = user,
                Project = project
            };

            await Data.Post("Tasks", task);
        }
    }
}
