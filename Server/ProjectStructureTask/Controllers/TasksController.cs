﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.DTOs;
using ProjectStructureTask.Services.Queries;
using Threading = System.Threading.Tasks;

namespace ProjectStructureTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;

        public TasksController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
        }

        // GET: api/Tasks
        [HttpGet]
        public async Threading.Task<IEnumerable<Task>> Get()
        {
            return await queryProcessor.ProcessAsync(new GetAllTasksQuery());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}", Name = "GetTask")]
        public async Threading.Task<Task> Get(int id)
        {
            return await queryProcessor.ProcessAsync(new GetTaskQuery { Id = id });
        }

        // POST: api/Tasks
        [HttpPost]
        public async Threading.Task<bool> Post([FromBody] AddTaskCommand command)
        {
            return await commandProcessor.ProcessAsync(command);
        }

        // PUT: api/Tasks/5
        [HttpPut("{id}")]
        public async Threading.Task<bool> Put(int id, [FromBody] UpdateTaskCommand command)
        {
            command.UpdateId = id;
            return await commandProcessor.ProcessAsync(command);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Threading.Task<bool> Delete(int id)
        {
            return await commandProcessor.ProcessAsync(new DeleteTaskCommand { Id = id });
        }
    }
}
