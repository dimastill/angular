﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructureTask.Services.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Queries;
using Threading = System.Threading.Tasks;

namespace ProjectStructureTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;

        public UsersController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
        }

        // GET: api/Users
        [HttpGet]
        public async Threading.Task<IEnumerable<User>> Get()
        {
            return await queryProcessor.ProcessAsync(new GetAllUsersQuery());
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetUser")]
        public async Threading.Task<User> Get(int id)
        {
            return await queryProcessor.ProcessAsync(new GetUserQuery { Id = id });
        }

        // POST: api/Users
        [HttpPost]
        public async Threading.Task<bool> Post([FromBody] AddUserCommand command)
        {
            return await commandProcessor.ProcessAsync(command);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Threading.Task<bool> Put(int id, [FromBody] UpdateUserCommand command)
        {
            command.UpdateId = id;
            return await commandProcessor.ProcessAsync(command);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Threading.Task<bool> Delete(int id)
        {
            return await commandProcessor.ProcessAsync(new DeleteUserCommand { Id = id });
        }
    }
}
