﻿using System.Collections.Generic;
using ProjectStructureTask.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Queries;
using Threading = System.Threading.Tasks;

namespace ProjectStructureTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;

        public TeamsController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
        }

        // GET: api/Teams
        [HttpGet]
        public async Threading.Task<ICollection<Team>> Get()
        {
            return await queryProcessor.ProcessAsync(new GetAllTeamsQuery());
        }

        [Route("oldusers")]
        [HttpGet]
        public async Threading.Task<IEnumerable<object>> GetOldUsers()
        {
            return await queryProcessor.ProcessAsync(new GetOldUsersQuery());
        }


        // GET: api/Teams/5
        [HttpGet("{id}", Name = "GetTeam")]
        public async Threading.Task<Team> Get(int id)
        {
            return await queryProcessor.ProcessAsync(new GetTeamQuery { Id = id });
        }
        // POST: api/Teams
        [HttpPost]
        public async Threading.Task<bool> Post([FromBody] AddTeamCommand command)
        {
            return await commandProcessor.ProcessAsync(command);
        }

        // PUT: api/Teams/5
        [HttpPut("{id}")]
        public async Threading.Task<bool> Put(int id, [FromBody] UpdateTeamCommand command)
        {
            command.UpdateId = id;
            return await commandProcessor.ProcessAsync(command);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Threading.Task<bool> Delete(int id)
        {
            return await commandProcessor.ProcessAsync(new DeleteTeamCommand { Id = id });
        }
    }
}
