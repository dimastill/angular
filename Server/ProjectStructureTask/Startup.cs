﻿﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Handlers;
using ProjectStructureTask.Services.Implementation;
using ProjectStructureTask.Services.Repositories;
using Microsoft.EntityFrameworkCore;
using ProjectStructureTask.Services.DTOs;

namespace ProjectStructureTask
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var mapper = MapperConfiguration().CreateMapper();
            services.AddScoped(_ => mapper);

            services.AddSingleton<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IToDoQueryHandler, ToDoQueryHandler>();
            services.AddTransient<IToDoCommandHandler, ToDoCommandHandler>();
            services.AddTransient<ICommandProcessor, ToDoCommandProcessor>();
            services.AddTransient<IQueryProcessor, ToDoQueryProcessor>();
            
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(c => c.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            
            app.UseMvc();
        }

        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AddProjectCommand, Project>();
                cfg.CreateMap<AddTeamCommand, Team>();
                cfg.CreateMap<AddUserCommand, User>();
                cfg.CreateMap<AddTaskCommand, Task>();
                cfg.CreateMap<UpdateProjectCommand, Project>();
                cfg.CreateMap<UpdateTeamCommand, Team>();
                cfg.CreateMap<UpdateUserCommand, User>();
                cfg.CreateMap<UpdateTaskCommand, Task>();
            });

            return config;
        }
    }
}
