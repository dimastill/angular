﻿using NUnit.Framework;
using Moq;
using ProjectStructureTask.Services.DTOs;
using ProjectStructureTask.Tests.Fake;
using System.Collections.Generic;
using Threading = System.Threading.Tasks;
using ProjectStructureTask.Services.Handlers;
using System;
using ProjectStructureTask.Services.Repositories;
using ProjectStructureTask.Services.Queries;
using ProjectStructureTask.Services.Command;
using AutoMapper;
using ProjectStructureTask.Controllers;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Implementation;
using Microsoft.AspNetCore.TestHost;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Diagnostics;

namespace ProjectStructureTask.Tests
{
    class IntegrationTest
    {
        private TestServer _server;
        private HttpClient _client;


        [SetUp]
        public void Setup()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        [Test]
        public async Threading.Task Test1()
        {
            // Arrange
            Team team = new Team { Id = 1, CreatedAt = DateTime.Now, Name = "TeamName" };
            User author = new User
            {
                Id = 2,
                FirstName = "Name",
                LastName = "LastName",
                RegisteredAt = DateTime.Now,
                Birthday = DateTime.Now,
                Email = "emailtest@email.com",
                Team = team
            };
            Project newProject = new Project
            {
                Id = 4,
                Name = "ProjectName",
                Description = "Decsription",
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now,
                Team = team,
                Author = author
            };

            string jsonObject = JsonConvert.SerializeObject(newProject);

            var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync($"/api/project", content);

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Test]
        public async Threading.Task Test2()
        {            
            // Act
            var response = await _client.DeleteAsync($"/api/users/1");

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Test]
        public async Threading.Task Test3()
        {
            // Arrange
            Team team = new Team { Id = 1, CreatedAt = DateTime.Now, Name = "TeamName" };
            
            string jsonObject = JsonConvert.SerializeObject(team);

            var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync($"/api/teams", content);

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Test]
        public async Threading.Task Test4()
        {
            // Act
            var response = await _client.GetAsync($"/api/users/sorted");
            var users = JsonConvert.DeserializeObject<IEnumerable<Task>>(await response.Content.ReadAsStringAsync());

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Test]
        public async Threading.Task Test5()
        {
            // Act
            var response = await _client.DeleteAsync($"/api/tasks/2");

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}
