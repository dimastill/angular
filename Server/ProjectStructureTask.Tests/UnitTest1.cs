using NUnit.Framework;
using Moq;
using ProjectStructureTask.Services.DTOs;
using ProjectStructureTask.Tests.Fake;
using System.Collections.Generic;
using Threading = System.Threading.Tasks;
using ProjectStructureTask.Services.Handlers;
using System;
using ProjectStructureTask.Services.Repositories;
using ProjectStructureTask.Services.Queries;
using ProjectStructureTask.Services.Command;
using AutoMapper;

namespace Tests
{
    public class Tests
    {
        private readonly FakeUnitOfWork fakeUnitOfWork;


        [SetUp]
        public void Setup()
        {
        }

        [TearDown]
        public void TestTearDown()
        {
            fakeUnitOfWork.Teams.Clear();
            fakeUnitOfWork.Users.Clear();
            fakeUnitOfWork.Projects.Clear();
            fakeUnitOfWork.Tasks.Clear();
        }

        [Test]
        public async Threading.Task Handle_When_Get_Number_Tasks_In_Project_Then_Result_Correctly_Return()
        {
            //Arrange            
            var unitOfWork = new Mock<UnitOfWork>();
            ToDoQueryHandler handler = new ToDoQueryHandler(unitOfWork.Object);

            var query = new Mock<GetNumberTasksInProjectQuery>();
            query.Object.Id = 1;

            //Act
            var result = handler.GetHandlers()[typeof(GetNumberTasksInProjectQuery)](query.Object);

            //Assert
            Assert.IsNotNull(await result);
        }

        [Test]
        public async Threading.Task Handle_When_Get_List_Tasks_Then_Result_Not_Null()
        {
            //Arrange            
            var unitOfWork = new Mock<UnitOfWork>();
            ToDoQueryHandler handler = new ToDoQueryHandler(unitOfWork.Object);

            var query = new Mock<GetListTasksQuery>();
            query.Object.Id = -1;

            //Act
            var result = handler.GetHandlers()[typeof(GetListTasksQuery)](query.Object);

            //Assert
            Assert.That(await result, Is.Empty);
        }

        [Test]
        public async Threading.Task Handler_When_Get_List_Tasks_Finished_2019_Then_Result_Return_Unique_Items()
        {
            //Arrange            
            var unitOfWork = new Mock<UnitOfWork>();
            ToDoQueryHandler handler = new ToDoQueryHandler(unitOfWork.Object);

            var query = new Mock<GetListTasksFinished2019Query>();
            query.Object.Id = 2;

            //Act
            var result = handler.GetHandlers()[typeof(GetListTasksFinished2019Query)](query.Object);

            //Assert
            Assert.That(await result, Is.Unique);
        }

        [Test]
        public async Threading.Task Handler_When_Get_Old_Users_Then_Result_Return_Items()
        {
            //Arrange            
            var unitOfWork = new Mock<UnitOfWork>();
            ToDoQueryHandler handler = new ToDoQueryHandler(unitOfWork.Object);

            var query = new Mock<GetOldUsersQuery>();

            //Act
            var result = handler.GetHandlers()[typeof(GetOldUsersQuery)](query.Object);

            //Assert
            Assert.That(await result, Is.Not.Empty);
        }

        [Test]
        public async Threading.Task Handler_When_Get_Sorted_Users_List_Then_Return_Exactly_4_Items()
        {
            //Arrange            
            var unitOfWork = new Mock<UnitOfWork>();
            ToDoQueryHandler handler = new ToDoQueryHandler(unitOfWork.Object);

            var query = new Mock<GetSortedUsersListQuery>();

            //Act
            var result = handler.GetHandlers()[typeof(GetSortedUsersListQuery)](query.Object);

            //Assert
            Assert.That(await result, Has.Exactly(4).Items);
        }

        [Test]
        public async Threading.Task Handle_When_Get_Info_About_Last_Project_Then_Return_Not_Null()
        {
            //Arrange            
            var unitOfWork = new Mock<UnitOfWork>();
            ToDoQueryHandler handler = new ToDoQueryHandler(unitOfWork.Object);

            var query = new Mock<GetInfoAboutLastProjectQuery>();
            query.Object.Id = 1;

            //Act
            var result = handler.GetHandlers()[typeof(GetInfoAboutLastProjectQuery)](query.Object);

            //Assert
            Assert.IsNotNull(await result);
        }

        [Test]
        public async Threading.Task Handler_When_Get_Info_About_Project_Then_Return_Not_Null()
        {
            //Arrange            
            var unitOfWork = new Mock<UnitOfWork>();
            ToDoQueryHandler handler = new ToDoQueryHandler(unitOfWork.Object);

            var query = new Mock<GetInfoAboutProjectQuery>();
            query.Object.Id = 1;
            //Act
            var result = handler.GetHandlers()[typeof(GetInfoAboutProjectQuery)](query.Object);

            //Assert
            Assert.IsNotNull(await result);
        }

        [Test]
        public async Threading.Task Handler_When_Add_User_Then_Return_True()
        {
            //Arrange   
            var unitOfWork = new Mock<UnitOfWork>();
            var mapConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<AddUserCommand, User>();
                cfg.CreateMap<User, AddUserCommand>();
            });
            var mapper = new Mock<Mapper>(mapConfig);
            ToDoCommandHandler handler = new ToDoCommandHandler(unitOfWork.Object, mapper.Object);
            
            var query = new AddUserCommand
            {
                Id = 4,
                FirstName = "MyName",
                LastName = "MyLastName",
                RegisteredAt = DateTime.Now,
                Email = "MyEmail@email.com",
                Birthday = DateTime.Parse("24.01.1999"),
                Team = unitOfWork.Object.Teams[0]
            };

            //Act
            var result = handler.GetHandlers()[typeof(AddUserCommand)](query);

            //Assert
            Assert.IsTrue((await result) as bool?);
        }

        [Test]
        public async Threading.Task Handlers_When_Update_State_Task_Then_Return_True()
        {
            //Arrange                 
            var unitOfWork = new Mock<UnitOfWork>();
            var mapConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<UpdateTaskCommand, Task>();
                cfg.CreateMap<Task, UpdateTaskCommand>();
            });
            var mapper = new Mock<Mapper>(mapConfig);
            ToDoCommandHandler handler = new ToDoCommandHandler(unitOfWork.Object, mapper.Object);

            var updateCommand = new UpdateTaskCommand {   UpdateId = 1, Description = "DescriptionTest0",
                Name = "NameTest0", CreateAt = DateTime.Parse("25.08.1987"), FinishedAt = DateTime.Parse("05.11.1988"),
                Perfomer = unitOfWork.Object.Users[0], Project = unitOfWork.Object.Projects[0],
                State = new TaskStateModel { Id = 1, Value = "Started" }
            };

            //Act
            var result = handler.GetHandlers()[typeof(UpdateTaskCommand)](updateCommand);

            //Assert
            Assert.IsTrue(await result as bool?);
        }

        [Test]
        public async Threading.Task Handlers_When_Update_User_Team_Then_Return_True()
        {
            //Arrange                 
            var unitOfWork = new Mock<UnitOfWork>();
            var mapConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<UpdateUserCommand, User>();
                cfg.CreateMap<User, UpdateUserCommand>();
            });
            var mapper = new Mock<Mapper>(mapConfig);
            ToDoCommandHandler handler = new ToDoCommandHandler(unitOfWork.Object, mapper.Object);

            var query = new Mock<UpdateUserCommand>();
            var updateCommand = new UpdateUserCommand { Id = 3, UpdateId = 3, FirstName = "FirstNameTest1",
                LastName = "LastNameTest1", Email = "EmailTest1@email.com", Birthday = DateTime.Parse("12.01.2005"),
                Team = unitOfWork.Object.Teams[0]
            };

            //Act
            var result = handler.GetHandlers()[typeof(UpdateUserCommand)](updateCommand);

            //Assert
            Assert.IsTrue(await result as bool?);
        }
    }
}